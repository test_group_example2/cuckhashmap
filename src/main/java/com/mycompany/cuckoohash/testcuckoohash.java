/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.cuckoohash;

/**
 *
 * @author Kitty
 */
public class testcuckoohash {

    public static void main(String[] args) {
        cuckoohash cuckoo = new cuckoohash();
        cuckoo.put(100, "Kitty");
        cuckoo.put(3, "Tanadon");
        cuckoo.put(101, "yoda");
        cuckoo.put(198, "mama");
        System.out.println(cuckoo.get(100).toString());
        System.out.println(cuckoo.get(3).toString());
        System.out.println(cuckoo.get(101).toString());
        System.out.println(cuckoo.get(198).toString());
        cuckoo.delete(3);
        System.out.println(cuckoo.get(3));
    }
}

